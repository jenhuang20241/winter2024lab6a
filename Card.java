public class Card
{
	private String suit;
	private String value;
	
	
	//constructor
	public Card (String suit, String value)
	{
		this.suit = suit;
		this.value = value;
	}

	//get methods
	public String getSuit()
	{
		return this.suit;
	}
	public String getValue()
	{
		return this.value;
	}
	
	//toString method
	public String toString()
	{
		return this.value + " of " + this.suit;
	}

}