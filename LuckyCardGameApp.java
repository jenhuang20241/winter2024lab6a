import java.util.Scanner;

public class LuckyCardGameApp
{
	public static void main (String[] args)
	{
		Scanner reader = new Scanner(System.in);
		
		
		// Lab6B
		GameManager manager = new GameManager();
		int totalPoints = 0;
		
		System.out.println("Welcome to the Card Game!");
		
		//card game loop
		while (manager.getNumberOfCards() > 1 && totalPoints < 5)
		{
			System.out.println(manager.toString());
			
			System.out.println("Points Awarded: " + manager.calculatePoints());
			
			totalPoints += manager.calculatePoints();
			System.out.println("Player points: " + totalPoints);
			
			manager.dealCards();
		}
		
		System.out.println("-----------------------------");
		System.out.println("Player points: " + totalPoints);
		
		if (totalPoints < 5) {
			System.out.println("Player Loses :/");
		}
		else {
			System.out.println("Player Wins!");
		}
	}
}