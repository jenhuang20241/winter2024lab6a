import java.util.Random;


public class Deck 
{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//constructor: initializes the deck
	public Deck()
	{
		this.numberOfCards = 52;
		this.cards = new Card[numberOfCards];
		this.rng = new Random();
		
		String[] suits = {"\u001B[31m"+"Hearts"+"\u001B[0m", "Spades", "Clubs", "\u001B[31m"+"Diamonds"+"\u001B[0m"};
		
		String[] values = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
		
		int deckIndex = 0;
		for(int j=0; j<suits.length; j++)
		{
			for(int i=0; i<values.length; i++)
			{
				this.cards[deckIndex] = new Card(suits[j], values[i]);
				deckIndex++;
			}
		}
	}
	
	
	//length()
	public int length()
	{
		return this.numberOfCards;
	}
	
	
	//drawTopCard()
	public Card drawTopCard()
	{
		Card lastCard = cards[numberOfCards-1];
		this.numberOfCards--;
		return lastCard;
	}
	
	
	//toString
	public String toString()
	{
		String result = "";
		
		for(int i=0; i<numberOfCards; i++)
		{
			result += this.cards[i] + "\n";
		}
		return result;
	}
	
	
	//shuffle()
	public void shuffle()
	{
		for (int i=0; i<numberOfCards; i++)
		{
			int randomIdx = rng.nextInt(this.numberOfCards - 1);
			Card temp = cards[i];
			cards[i] = cards[randomIdx];
			cards[randomIdx] = temp;
		}
	}
}