public class GameManager 
{

	//fields
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//constructor
	public GameManager()
	{
		this.drawPile = new Deck();
		drawPile.shuffle();
		
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public String toString()
	{
		return "----------------" +
		"\nCenter card: " + this.centerCard +
		"\nPlayer card: " + this.playerCard +
		"\n------------------";
	}
	
	public void dealCards()
	{
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public int getNumberOfCards()
	{
		return drawPile.length();
	}
	
	public int calculatePoints()
	{
		int result;
		
		if (centerCard.getValue() == playerCard.getValue())
		{
			result = 4;
		} 
		else if (centerCard.getSuit() == playerCard.getSuit())
		{
			result = 2;
		}
		else
		{
			result = -1;
		}
		
		return result;
	}

}